---
title: Modèle de tricot d'Hitomezashi
---
# Modèle de tricot d'Hitomezashi 

## Contexte

Suite à la vidéo de `Numberphile` sur [Youtube](https://www.youtube.com/watch?v=JbfhzlMk2eY&list=PLOET8VOdc8vd4nTUQiq1kjXHk94Vrp8_g&index=14), je me suis lancé dans quelques tracés sur papier.  
La famille a été mise a contibution, puis j'ai eu envie d'automatiser le processus pour faire de grands motifs. C'est ainsi que ces cellules de jupyter-notebook sont nées.


```python
# Quelques imports
import matplotlib.pyplot as plt
from matplotlib import collections  as mc
from math import pi, sqrt
from mpmath import mp
```

Lien vers la documentation de [LineCollection](https://matplotlib.org/stable/api/collections_api.html#matplotlib.collections.LineCollection), un module pour tracer des segments avec `matplotlib`.

## Décomposons notre problème

**Etape 1 :** Générer simplement des segments horizontaux ou verticaux de taille 1 -> `gene_segment`  
**Etape 2 :** Générer simplement des lignes pointillées de taille `n` horizontales ou verticales -> `gene_pointilles`


```python
def gene_segment(i,j,orientation = 'horiz'):
    if orientation == "horiz":
        return [(i,j),(i+1,j)]
    return [(i,j),(i,j+1)]

assert gene_segment(2,3,'horiz') == [(2,3),(3,3)]
assert gene_segment(5,2,'horiz') == [(5,2),(6,2)]
assert gene_segment(2,3,'vert') == [(2,3),(2,4)]
assert gene_segment(5,2,'vert') == [(5,2),(5,3)]

def gene_pointilles(i,j,n,orientation):
    pointilles = []
    while i < n and j < n :
        pointilles.append( gene_segment(i,j,orientation) )
        if orientation == 'horiz' :
            i += 2
        else :
            j += 2
    return pointilles
                
# test1 = gene_pointilles(0,0,10,'horiz')
# print(test1)
# test2 = gene_pointilles(1,3,10,'horiz')
# print(test2)
```

## Générer des motifs à partir de nombres "célèbres"

Pour obtenir un premier motif assez grand, je me suis servi des décimales de $\pi$ (en ligne) et de $\sqrt{2}$ (en colonne).  
La cellule ci-dessous, génère deux listes contenant les décimales de ces deux nombres.


```python
n = 200
mp.dps = n+5    # set number of digits

lignes = [int(i) for i in str(mp.pi)[0:n+1] if i != '.']
colonnes = [int(i) for i in str(mp.sqrt(2))[0:n+1] if i != '.']

print(len(lignes),len(colonnes))
```

    200 200
    

## Générer les segments

La fonction `trace` ci-dessous va permettre de générer la liste des coordonnées des extrémités des segments à tracer.  
La syntaxe de `LineCollection` pour tracer deux segments $[AB]$ et $[CD]$ est : `[ [(x_A,y_A) , (x_B,y_B)] , [(x_C,y_C) , (x_D,y_D)] ]`


```python
def trace(lignes,colonnes):
    segments = []
    for j in range(n):
        if lignes[j]%2 == 0:
            segments = segments + gene_pointilles(0,j,n,'horiz')
        else :
            segments = segments + gene_pointilles(1,j,n,'horiz')
    for i in range(n):
        if lignes[i]%2 == 0:
            segments = segments + gene_pointilles(i,0,n,'verti')
        else :
            segments = segments + gene_pointilles(i,1,n,'verti')
    return segments

segments = trace(lignes,colonnes)

#  Affichage des coordonnées des 20 premiers segments
print(segments[0:20])
```

    [[(1, 0), (2, 0)], [(3, 0), (4, 0)], [(5, 0), (6, 0)], [(7, 0), (8, 0)], [(9, 0), (10, 0)], [(11, 0), (12, 0)], [(13, 0), (14, 0)], [(15, 0), (16, 0)], [(17, 0), (18, 0)], [(19, 0), (20, 0)], [(21, 0), (22, 0)], [(23, 0), (24, 0)], [(25, 0), (26, 0)], [(27, 0), (28, 0)], [(29, 0), (30, 0)], [(31, 0), (32, 0)], [(33, 0), (34, 0)], [(35, 0), (36, 0)], [(37, 0), (38, 0)], [(39, 0), (40, 0)]]
    

## La touche finale : le tracé !

Il ne reste plus qu'à donner les bonnes instructions pour obtenir le tracé de l'ensemble des segments obtenus à l'étape précédente.  
La syntaxe `mc.LineCollection(segments, linewidths=2)` avec `segments` qui est la liste des coordonnées.  J'ai ajouté un bord en noir pour délimiter le motif.

**Ci-dessous, le motif d'Hitomezashi obtenu avec $\pi$ et $\sqrt{2}$ avec 200 décimales** 


```python
# lines = [[(0, 1), (1, 1)], [(2, 3), (3, 3)], [(1, 2), (1, 3)]]
# lines = test1 + test2
lc = mc.LineCollection(segments, linewidths=2)
bords = mc.LineCollection([[(0,0),(n,0)],[(0,0),(0,n)],[(n,n),(n,0)],[(n,n),(0,n)]], linewidths=2, color = '0')

fig, ax = plt.subplots(figsize=(n//4, n//4))
ax.add_collection(lc)
ax.add_collection(bords)
ax.autoscale()
ax.margins(0.1)

plt.savefig('test1.png',dpi=150)
```


    
![png](/hexo/images/Hitomezashi-raw.png)

## Bonus : le motif précédent coloré

Je me suis ensuite "amusé" avec `paint.net` pour colorer ce motif avec 2 couleurs. Voici le résultat :


![png](/hexo/images/Hitomezashi-couleur1.png)



```python

```
