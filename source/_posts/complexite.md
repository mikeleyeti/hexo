---
title: Complexité
---

## Objectif
Comprendre ce que signifie P = NP  ?  
P : les problèmes que l'on peut décider en temps polynomial.  
NP : les problèmes dont on peut vérifier les solutions en temps polynomial.  

## Méthodologie
Sur des automates finis, voir un peu ce problème

### Automates fini :

Soit A un automate fini d'ensemble d'état Q  
etat_moitie(A,n) peut prendre au plus |Q| valeurs (qui est fini).

D'après le théorème des tiroirs, il existe m et n tels que   
etat_moitie(A,n) = etat_moitie(A,m)  

A accepte a^nb^m ssi A accepte a^nb^n  
A ne reconnait donc pas le language équilibré  
Le language équilibré n'est pas reconnaissable par un automate fini.  
