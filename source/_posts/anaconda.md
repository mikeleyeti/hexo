---
title: Anaconda
---
## Présentation

Anaconda est une distribution Python avec plusieurs bibliotèques (plus de 1500)
intégrées dès l'installation. 

Une interface utilisateur permet d'utiliser les outils préintégrés parmi lesquels :
- Jupter Notebook;
- Jupyter Lab (qui est l'évolution de Jupyter Notebook);
- Syper : un environnement de développement orienté "Data Science".

## Installation

[Vers le site](https://www.anaconda.com/products/individual)

## Usage

