---
title: Hello World
---
Welcome to [Hexo](https://hexo.io/)! This is your very first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new "My New Post"
```

More info: [Writing](https://hexo.io/docs/writing.html)

### Run server

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)

### Test de code Python :
``` python
carre3 = [[2, 7, 6],
          [9, 5, 1],
          [4, 3, 8]]

carre4 = [[4, 5, 11, 14],
          [15, 10, 8, 1],
          [7, 3, 13, 12],
          [9, 16, 2, 6]]


def somme_ligne(carre : list,n : int):
    """
    carre est un tableau carré d'entier naturels
    n est un entier inférieur au nombre de lignes de carre
    """
    somme = 0
    for nombre in carre[n]:
        somme = somme + nombre
    return somme
```

