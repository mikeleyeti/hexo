---
title: Programmation dynamique
---
## Introduction
Résoudre un problème en subdivisant en sous-problèmes  
Les solutions des sous-problèmes sont enregistrées pour éviter de les résoudre plusieurs fois.

## Fibonacci
### Version itérative :

Version 1
``` python
def fibo(n):
    calculs = [1,1]
    for _ in range(2,n):
        resultat = calculs[-1]+calculs[-2]
        calculs.append(resultat)
    return calculs[-1]
```
Temps O(n) et mémoire O(n)

Version 2
``` python
def fibo(n):
    X = 0
    Y = 1
    for _ in range(2,n):
        Z = X + Y
        X = Y
        Y = Z
    return Z
```
Temps O(n) et mémoire O(1)

## Programmation dynamique

Indices de problèmes suceptibles d'être résolu avec la prog. dynamique :
- La solution du problème peut s'obtenir avec les solutions des sous-problèmes
- Un algo récursif calculera encore et encore les mêmes sous-problèmes

Deux approches : 
- approche descendante
- approche ascendante

## Le problème du rendu de monnaie




