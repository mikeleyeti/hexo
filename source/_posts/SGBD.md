---
title: SGBD
---
## SGBD -> MLD
Etapes :
- Une table par entité
- Pour les relations de types 0N - ON ( ou 1N - ON) 
créer une table dont la clé est la concaténation des clés des entités en relation
- Pour les relations avec un côté 0-1 ou 1-1, on recopie l'identifiant du côté 0N vers la table du côté 11.

### Exemples :

|  Client     |
|-------------|
|  **numcli** |
| nomcli      |
| prenomcli   |

|  COMMANDER  |
|-------------|
|  **numcli** |
| **numprod** |
| quantite   |

|  PRODUIT     |
|-------------|
|  **numprod** |
| nomprod      |
| PUprod   |


| PRODUIT    |   | RAYON     |
|------------|---|-----------|
| **numpro** |   | **idray** |
| nompro     |   | numRay    |
| PUprod     |   |           |
| *idray*    |   |           |


